import 'package:flutter/material.dart';
import 'package:resep_makanan_api/model/meal.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:resep_makanan_api/view/details.dart';

class Seafood extends StatefulWidget {
  @override
  _SeafoodState createState() => _SeafoodState();
}

class _SeafoodState extends State<Seafood> {
  List<Meal> meals = [];

  void _showSnackBar(BuildContext context, int index) {
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text(meals[index].strMeal),
        duration: Duration(seconds: 1),
      )
    );
  }
  
  @override 
  void initState() {
    super.initState();
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: getBody(),
    );
  }

  getBody() {
    if(meals.length <= 0) {
      return new Center(
        child: new CircularProgressIndicator(),
      );
    } else {
      return getGridView();
    }
  }

  GridView getGridView() => new GridView.builder(
    itemCount: meals.length,
    gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
    itemBuilder: (BuildContext context, int index) {
      return getCard(index);
    },
  );

  Widget getCard(int index) {
    return new Hero(
        tag: meals[index].idMeal,
        child: new GestureDetector(
        onTap: () {
          _showSnackBar(context, index);
          Navigator.push(context, 
            MaterialPageRoute(
              builder: (context) => MealDetailsPage(meal: meals[index],)
            ),
          );
        },
        child: MealCard(meal: meals[index]),
      ),
    );
  }

  loadData() async {
    String dataUrl = "https://www.themealdb.com/api/json/v1/1/filter.php?c=Seafood";
    http.Response response = await http.get(dataUrl);
    var responseJson = json.decode(response.body);
    if (response.statusCode == 200) {
      print("fetching data seafood...");
      setState(() {
        meals = (responseJson['meals'] as List)
        .map((p) => Meal.fromJson(p))
        .toList();
        print("update state seafood");
      }); 
    } else {
      throw Exception("Failed to load json");
    }
  } 
}