import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:resep_makanan_api/model/meal.dart';
import 'package:resep_makanan_api/model/meal_details.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class MealDetailsPage extends StatefulWidget {
  MealDetailsPage({Key key, this.meal}) : super(key: key);
  final Meal meal;

  @override
  _MealDetailsPageState createState() => _MealDetailsPageState(this.meal);
}

class _MealDetailsPageState extends State<MealDetailsPage> {
  _MealDetailsPageState(this.meal);
  final Meal meal;
  MealDetails mealDetails;

  @override 
  void initState() {
    super.initState();
    loadData();
  }

  Widget _flightShuttleBuilder(
    BuildContext flightContext,
    Animation<double> animation,
    HeroFlightDirection flightDirection,
    BuildContext fromHeroContext,
    BuildContext toHeroContext,
  ) {
    return DefaultTextStyle(
      style: DefaultTextStyle.of(toHeroContext).style,
      child: toHeroContext.widget,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('Detail Bahan Makanan'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, false),
        ),
      ),
      body: getBody(),
    );
  }  

  getBody() {
    if (mealDetails == null) {
      return new Center(
        child: new CircularProgressIndicator(),
      );
    } else {
      final TextStyle textStyle = Theme.of(context).textTheme.display1;
      return new Center(
        child: Hero(
          tag: mealDetails.idMeal,
          flightShuttleBuilder: _flightShuttleBuilder,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Image.network(mealDetails.strMealThumb),
                Text(mealDetails.strMeal, style: textStyle),
                Text(mealDetails.strInstructions),
              ],
            ),
          ),
        ),
      );
    }
  }

  loadData() async {
    String dataUrl = "https://www.themealdb.com/api/json/v1/1/lookup.php?i=${meal.idMeal}";
    http.Response response = await http.get(dataUrl);
    var responseJson = json.decode(response.body);
    print("fetching details id ${meal.idMeal}...");
    if (response.statusCode == 200) {
      setState(() {
        var mealDetailsJson = responseJson['meals'][0];
        mealDetails = MealDetails.fromJson(mealDetailsJson);
        print("update state details");
      }); 
    } else {
      throw Exception("Failed to load json");
    }
  }
}