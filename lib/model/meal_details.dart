class MealDetails {
  final String idMeal;
  final String strMeal;
  final String strMealThumb;
  final String strInstructions;

  MealDetails(this.idMeal, this.strMeal, this.strMealThumb, this.strInstructions);

  factory MealDetails.fromJson(Map<String, dynamic> json) {
    return MealDetails(json['idMeal'], json['strMeal'], json['strMealThumb'], json['strInstructions']);
  }
}

