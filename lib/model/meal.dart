import 'package:flutter/material.dart';

class Meal {
  final String idMeal;
  final String strMeal;
  final String strMealThumb;
  
  Meal(this.idMeal, this.strMeal, this.strMealThumb);
  
  factory Meal.fromJson(Map<String, dynamic> json) {
    return Meal(json['idMeal'], json['strMeal'], json['strMealThumb']);
  }
}

class MealCard extends StatelessWidget {
  const MealCard({Key key, this.meal}) : super(key: key);
  final Meal meal;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Image.network(meal.strMealThumb),
            ),
            Text(meal.strMeal),
          ]
        ),
      )
    );
  }
}